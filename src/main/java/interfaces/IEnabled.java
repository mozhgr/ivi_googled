package interfaces;

public interface IEnabled {
    boolean isEnabled();
}
