package interfaces;

public interface HasText {
    String getText();
}
