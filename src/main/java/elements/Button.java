package elements;

import interfaces.Clickable;
import interfaces.HasText;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;

/**
 * Кастомный класс кнопки со своими ожиданием и логированием
 */
public class Button extends AbstractElement implements HasText, Clickable {

    public Button(WebElement initialElement) {
        super(initialElement);
    }
    private static Logger logger = Logger.getLogger(Button.class);


    /**
     * Переопределение метода клика по элементу
     */
    @Override
    public void click() {
        try {
            logger.info(String.format("Клик по кнопке '%s'", name));
            if (initialElement.isEnabled()) initialElement.click();
        } catch (Exception e) {
            logger.error("Не удалось выполнить клик по кнопке", e);
        }
    }

    /**
     * Метод получения текста элемента
     */
    public String getText() {
        return initialElement.getText();
    }
}
