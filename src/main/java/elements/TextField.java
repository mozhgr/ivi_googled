package elements;

import interfaces.Clickable;
import interfaces.HasText;
import interfaces.TextEditable;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;

import static com.sun.xml.internal.fastinfoset.stax.events.Util.isEmptyString;


/**
 * Кастомный класс текстового поля
 */
public class TextField extends AbstractElement implements HasText, TextEditable, Clickable {

    private static Logger logger = Logger.getLogger(TextField.class);

    public TextField(WebElement initialElement) {
        super(initialElement);
    }

    public TextField(WebElement initialElement, String fieldName) {
        super(initialElement);
        name = fieldName;
    }

    /**
     * Метод получения текста элемента
     */
    public String getText() {
        String text = getAttribute("value");
        return (isEmptyString(text)) ? initialElement.getText() : text;
    }

    /**
     * Метод ввода текста в поле
     */
    public void setText(String text) {
        logger.info(String.format("Ввод текста '%s' в поле '%s'", text, name));
        try {
            if (!isEnabled()) {
                logger.info("Текстовое поле в состоянии \"отключено\", ввод текста не возможен");
            }
            initialElement.clear();
            initialElement.click();
            Thread.sleep(1000);
            initialElement.sendKeys(text);
        } catch (Exception e) {
            logger.info("Не удалось ввести текст", e);
        }
    }

    /**
     * Метод добавления текста в поле без очистки
     */
    public void appendText(String text) {
        initialElement.sendKeys(text);
    }
}
