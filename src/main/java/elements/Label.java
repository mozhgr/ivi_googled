package elements;

import interfaces.Clickable;
import interfaces.HasText;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;

/**
 * Кастомный класс Лэйбла
 */
public class Label extends AbstractElement implements HasText, Clickable {

    private static Logger logger = Logger.getLogger(Label.class);

    public Label(WebElement initialElement) {
        super(initialElement);
    }

    public String getText() {
        return isDisplayed() ? initialElement.getText() : "";
    }

    public String getValue() {
        return getAttribute("value");
    }
}
