package elements;

import interfaces.IEnabled;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import utils.DriverUtils;

import java.util.Objects;

/**
 * Абстрактный класс для кастомных элементов.
 */
public abstract class AbstractElement implements IEnabled{

    private static Logger logger = Logger.getLogger(AbstractElement.class);
    WebElement initialElement;
    String name = null;


    /**
     * Конструктор класса
     */
    public AbstractElement(WebElement initialElement) {
        this.initialElement = initialElement;
        name = getObjectName();
    }

    /**
     * Метод проверки, что элемент отобразился с ожиданием
     */
    public boolean isDisplayed() {
        if (initialElement == null) {
            logger.info("Элемент не был проинициализирован и имеет значение null");
        }
        try {
            return DriverUtils.waitFor(0, ExpectedConditions.visibilityOf(initialElement)).isDisplayed();
        } catch (TimeoutException e) {
            return false;
        }
    }

    /**
     * Метод проверки, что элемент доступен
     */
    public boolean isEnabled() {
        return ifPresent().isEnabled();
    }

    /**
     * Метод проверки, что элемент проинициализирован
     */
    private WebElement ifPresent() {
        if (initialElement == null) {
            logger.info(String.format("Не проинициализирован элемент '%s'", name));
        }
        return initialElement;
    }

    /**
     * Метод получения имени объекта
     */
    String getObjectName() {
        try {
            if (name != null && !name.equals("")) {
                return name;
            }
            return ifPresent().getText().equals("") ? initialElement.findElement(By.xpath("..")).getText() : initialElement.getText();
        } catch (Exception ignored) {
            return "";
        }
    }

    /**
     * Метод клика по элементу
     */
    public void click() {
        Objects.requireNonNull(DriverUtils.waitForOrFail(3, ExpectedConditions.elementToBeClickable(ifPresent()))).click();
    }


    /**
     * Кастомный метод получения атрибута
     */
    public String getAttribute(String attribute) {
        String result = ifPresent().getAttribute(attribute);
        if (result != null && !result.equals("")) {
            return result;
        }
        if ("text".equals(attribute)) {
            return initialElement.getText();
        }
        if ("index".equals(attribute)) {
            return String.valueOf(initialElement.findElements(By.xpath("./../*")).indexOf(initialElement));
        }
        return null;
    }

}
