package config;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * Класс конфига
 */
public final class Config {

    private static final Properties CONFIG_PROPERTIES;
    private static Logger logger = Logger.getLogger(Config.class);
    private static final int SECOND = 1000;

    static {
        CONFIG_PROPERTIES = new Properties();
        try (InputStream configInputStream = new FileInputStream("src/main/resources/config.properties")) {
            CONFIG_PROPERTIES.load(configInputStream);
        } catch (IOException e) {
            logger.info("Unable to load configuration file", e);
            throw new IllegalStateException("Unable to load configuration file", e);
        }
    }

    /**
     * Метод получения глобального таймаута
     */
    public static int getGlobalTimeout() {
        return Integer.parseInt(CONFIG_PROPERTIES.getProperty("timeout.global")) * SECOND;
    }

    /**
     * Метод получения ссылки на точку входа на сайт
     */
    public static String getAppUrl() {
        return CONFIG_PROPERTIES.getProperty("app.url");
    }

    /**
     * Метод получения местоположения драйвера на диске
     */
    public static String getDriverLocation() {
        return CONFIG_PROPERTIES.getProperty("driver.location");
    }

}
