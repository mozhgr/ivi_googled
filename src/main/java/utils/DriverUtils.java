package utils;

import config.Config;
import elements.AbstractElement;
import exceptions.DriverNotInitializedException;
import org.apache.log4j.Logger;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

/**
 * Класс для работы с драйвером. В основном содержит ожидания
 */
public class DriverUtils {

    private static ThreadLocal<RemoteWebDriver> driverContainer = new ThreadLocal<>();

    public synchronized static RemoteWebDriver get() {
        return Optional.ofNullable(driverContainer.get()).orElseThrow(DriverNotInitializedException::new);
    }

    private static Logger logger = Logger.getLogger(DriverUtils.class);

    private DriverUtils() {
    }

    private static void disableImplicitWait() {
        get().manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
    }

    private static void enableImplicitWait() {
        get().manage().timeouts().implicitlyWait(Config.getGlobalTimeout(), TimeUnit.SECONDS);
    }

    public static <V> V waitFor(int seconds, Function<? super WebDriver, V> isTrue) {
        try {
            disableImplicitWait();
            return new WebDriverWait(get(), seconds).until(isTrue);
        } finally {
            enableImplicitWait();
        }
    }

    public static <V> V waitFor(Function<? super WebDriver, V> isTrue) {
        return new WebDriverWait(get(), Config.getGlobalTimeout()).until(isTrue);
    }

    public static <V> V waitForOrFail(int seconds, Function<? super WebDriver, V> isTrue) {
        try {
            return waitFor(seconds, isTrue);
        } catch (TimeoutException e) {
            logger.info("Не удалось дождаться события", e);
        }
        return null;
    }
}
