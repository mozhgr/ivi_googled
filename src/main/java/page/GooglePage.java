package page;

import elements.TextField;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Абстрактная пейджа google, от которой наследуются все страницы google.
 */
public abstract class GooglePage extends AbstractPage{

    @FindBy(xpath = "//form//div[contains(@jsaction, 'input')]/input")
    private WebElement search;
    private TextField searchInput;

    GooglePage(WebDriver driver) {
        super(driver);
        waitForLoadFinished();
    }

    /**
     * Метод поиска в google.
     * Включает в себя ввод поискового запроса в поле 'Поиск' и нажатие кнопки Enter
     */
    public void googleThat(String text) {
        searchInput = new TextField(search, "Поиск");
        searchInput.setText(text);
        search.sendKeys(Keys.ENTER);
    }
}
