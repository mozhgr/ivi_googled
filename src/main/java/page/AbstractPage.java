package page;

import config.Config;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;


/**
 * Абстрактная пейджа, от которой наследуются все остальные страницы.
 */
public abstract class AbstractPage {
    WebDriver driver;
    WebDriverWait wait;
    private static final int POLLING = 100;

    AbstractPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, Config.getGlobalTimeout(), POLLING);
        waitForLoadFinished();
        PageFactory.initElements(driver, this);
    }

    /**
     * Метод ожидания загрузки страницы
     */
    public void waitForLoadFinished() {
        new WebDriverWait(driver, 10000).until(
                webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
    }
}
