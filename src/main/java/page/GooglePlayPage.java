package page;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class GooglePlayPage extends AbstractPage {

    private static Logger logger = Logger.getLogger(GooglePlayPage.class);

    @FindBy(xpath = "//c-wiz/div/div[contains(@aria-label, 'Средняя оценка') and not(contains(@role, 'img'))]")
    private WebElement googlePlayIviRating;

    public GooglePlayPage(WebDriver driver) {
        super(driver);
        waitForLoadFinished();
    }

    public String getGooglePlayIviRating() {
        return googlePlayIviRating.getText();
    }

}
