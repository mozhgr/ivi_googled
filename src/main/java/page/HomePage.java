package page;

import config.Config;
import elements.Button;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

/**
 * Главная страница google.com
 */
public class HomePage extends GooglePage {

    @FindBy(xpath = "//a[contains(text(), 'Войти')]")
    private Button buttonLogIn;

    @FindBy(xpath = "//form/div/div/div/center/input[@name='btnK']")
    private Button buttonSearch;

    @FindBy(xpath = "//form/div/div/div/center/input[@jsaction='sf.lck']")
    private Button buttonLuck;


    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void openHomePage(){
        String url = Config.getAppUrl();
        driver.get(url);
    }

}
