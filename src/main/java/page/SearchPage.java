package page;

import elements.Button;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 * Страница с результатами поиска
 */
public class SearchPage extends GooglePage {

    private static Logger logger = Logger.getLogger(SearchPage.class);

    @FindBy(xpath = "//form//button[contains(@aria-label, 'Google')]")
    private WebElement search;
    private Button buttonSearch;

    @FindBy(xpath = "//div[@role='tab']/a[contains(@href, 'tbm=isch')]")
    private WebElement pics;
    private Button buttonPics;

    private List<WebElement> linksList;

    @FindBy(xpath = "//a[@id='pnnext']")
    private WebElement nextPage;
    private Button nextPageButton;

    @FindBy(xpath = "//a[contains(@href,'play.google.com')]/..//..//div[contains(text(), 'Рейтинг')]")
    private WebElement previewGooglePlayRating;

    public static String previewGooglePlayRatingText = "";


    public SearchPage(WebDriver driver) {
        super(driver);
        waitForLoadFinished();
    }

    /**
     * Переход на страницу 'Картинки'
     */
    public void clickPics() {
        buttonPics = new Button(pics);
        buttonPics.click();
    }

    public boolean searchLinkOnFirstFivePages(String link) {
        List<WebElement> linksList = driver.findElements(By.xpath("//a[contains(@href,'" + link + "')]"));

        if (linksList.size() > 0) {
            if (link.equals("play.google.com")) {
                previewGooglePlayRatingText = previewGooglePlayRating.getText();
            }
            linksList.get(0).click();
            logger.info("Открылась страница " + link);
            return true;
        } else {
            nextPageButton = new Button(nextPage);
            nextPageButton.click();
            return false;
        }

    }

    public String getPreviewGooglePlayRating() {
        return previewGooglePlayRatingText;
    }


}
