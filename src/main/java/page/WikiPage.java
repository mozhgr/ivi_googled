package page;

import elements.TextField;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.List;

public class WikiPage extends AbstractPage {

    private static Logger logger = Logger.getLogger(WikiPage.class);

    @FindBy(xpath = "//a[contains(@href, 'ivi.ru')]")
    private List<WebElement> iviLinkList;

    public WikiPage(WebDriver driver) {
        super(driver);
        waitForLoadFinished();
    }

    /**
     * Проверка, что в статье есть ссылка на официальный сайт ivi.ru
     */
    public void checkIviLinkExistence() {
        if (iviLinkList.size() > 0) {
            logger.info("В статье " + iviLinkList.size() + " ссылок на официальный сайт ivi.ru");
        } else {
            Assert.fail("В статье нет ссылок на официальный сайт ivi.ru");
        }
    }


}
