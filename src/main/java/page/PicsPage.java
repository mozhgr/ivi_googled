package page;

import elements.AbstractElement;
import elements.Button;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.List;

public class PicsPage extends SearchPage {

    private static Logger logger = Logger.getLogger(PicsPage.class);

    @FindBy(xpath = "//div[@id='top_nav']//div/div/a[@role='button']")
    private WebElement instruments;
    private Button buttonInstruments;

    @FindBy(xpath = "//div[@id='top_nav']//div[@id='hdtbMenus']/div/div[@role='button'][1]")
    private WebElement size;
    private Button buttonSize;

    @FindBy(xpath = "//div[@id='top_nav']//div[@id='hdtbMenus']/div/ul[1]/li[@id='isz_l']")
    private WebElement largeSize;
    private Button largeSizeButton;

    @FindBy(xpath = "//div[@id='search']//a/div/span")
    private List<WebElement> linksList;



    public PicsPage(WebDriver driver) {
        super(driver);
        waitForLoadFinished();
    }

    /**
     * Нажатие на кнопку 'Инструменты'
     */
    public void clickInstruments() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        buttonInstruments = new Button(instruments);
        buttonInstruments.click();
    }

    /**
     * Нажатие кнопки 'Размер'
     */
    public void clickButtonSize() {
        buttonSize = new Button(size);
        buttonSize.click();
    }

    /**
     * Нажатие кнопки 'Большие'
     */
    public void clickLargeSizeButton() {
        largeSizeButton = new Button(largeSize);
        largeSizeButton.click();
    }

    /**
     * Поиск ссылок на ivi.ru на первой странице результатов поиска
     */
    public void searchIviInLinksList() {
        int i = 0;
        for (WebElement link : linksList) {
            String linkText = link.getText();
            if (linkText.equals("ivi.ru")) {
                i++;
            }
        }
        if (i > 2) {
            logger.info(i + " ссылок на ivi.ru найдено на первой странице результатов поиска по картинкам");
        } else {
            Assert.fail("Ошибка: количество ссылок на ivi.ru на первой странице результатов поиска по картинкам - " + i);
        }

    }
}
