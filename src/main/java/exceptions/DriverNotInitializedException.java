package exceptions;


/**
 * Эксепшн, что драйвер не проинициализирован
 */
public class DriverNotInitializedException extends RuntimeException {

    public DriverNotInitializedException() {
        super("Driver has not been initialized yet");
    }
}
