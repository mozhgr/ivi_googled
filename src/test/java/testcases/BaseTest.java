package testcases;

import config.Config;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import page.*;

public class BaseTest {
    private WebDriver driver = null;
    private HomePage homePage = null;
    private SearchPage searchPage = null;
    private PicsPage picsPage = null;
    private WikiPage wikiPage = null;
    private GooglePlayPage googlePlayPage = null;
    private static Logger logger = Logger.getLogger(BaseTest.class);


    /**
     * Инициализация драйвера и открытие главной страницы google.com
     */
    @BeforeTest
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", Config.getDriverLocation());
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        logger.info("before");
        openHomePage();

    }

    /**
     * Метод открытия главной страницы google.com
     */
    public void openHomePage() {
        homePage = new HomePage(driver);
        homePage.openHomePage();
        homePage.waitForLoadFinished();
        logger.info("Открытие главной страницы google");
    }

    public void openSearchPage() {
        searchPage = new SearchPage(driver);
        searchPage.waitForLoadFinished();
        logger.info("Открылась страница поиска");
    }

    public void openPicsPage() {
        picsPage = new PicsPage(driver);
        picsPage.waitForLoadFinished();
        logger.info("Открылась страница Картинок");
    }


    /**
     * Метод, который на первых 5 страницах находит ссылку на статью в wikipedia об ivi и переходит на неё
     */
    public void searchWikiOnFirstFivePages() {
        for (int i = 0; i < 5; i++) {
            openSearchPage();
            if (searchPage.searchLinkOnFirstFivePages("wikipedia.org/wiki/Ivi.ru")) {
                wikiPage = new WikiPage(driver);
                wikiPage.waitForLoadFinished();
                break;
            }
        }
    }

    /**
     * Переход на страницу 'Картинки'
     */
    public void goToPicks() {
        searchPage.clickPics();
    }

    /**
     * Нажатие на кнопку 'Инструменты'
     */
    public void clickInstrumentsButton() {
        picsPage.clickInstruments();
    }

    /**
     * Нажатие на кнопку 'Размер'
     */
    public void clickButtonSize() {
        picsPage.clickButtonSize();
    }

    /**
     * Нажатие на кнопку 'Большие'
     */
    public void clickLargeSizeButton() {
        picsPage.clickLargeSizeButton();
    }

    /**
     * Поиск ссылок на ivi.ru на первой странице результатов поиска
     */
    public void searchIviInLinksList() {
        picsPage.searchIviInLinksList();
    }

    /**
     * Проверка, что в статье есть ссылка на официальный сайт ivi.ru
     */
    public void checkIviLinkExistence() {
        wikiPage.checkIviLinkExistence();
    }

    /**
     * Метод поиска 'ivi' в google
     */
    public void searchIvi() {
        homePage.googleThat("ivi");
    }

    /**
     * Метод, который на первых 5 страницах находит ссылку на приложение ivi в play.google.com и переходит по ней
     */
    public void searchGooglePlayOnFirstFivePages() {
        for (int i = 0; i < 5; i++) {
            openSearchPage();
            if (searchPage.searchLinkOnFirstFivePages("play.google.com")) {
                googlePlayPage = new GooglePlayPage(driver);
                googlePlayPage.waitForLoadFinished();
                break;
            }
        }
    }


    /**
     * Метод, который сравнивает, что рейтинг приложения на кратком контенте страницы совпадает с рейтингом при переходе в Google Play
     */
    public void compareGooglePlayIviRating() {
        String previewGooglePlayRating = searchPage.getPreviewGooglePlayRating();
        String googlePlayPageRating = googlePlayPage.getGooglePlayIviRating();
        if (previewGooglePlayRating.contains("Рейтинг: " + googlePlayPage.getGooglePlayIviRating() + " - ")) {
            logger.info("Рейтинг приложения на кратком контенте страницы (" + previewGooglePlayRating
                    + ") совпадает с рейтингом при переходе в Google Play (" + googlePlayPageRating + ")");
        } else {
            Assert.fail("Ошибка: рейтинг приложения на кратком контенте страницы (" + previewGooglePlayRating
                    + ") не совпадает с рейтингом при переходе в Google Play (" + googlePlayPageRating + ")");
        }
    }

    /**
     * Закрытие драйвера
     */
    @AfterTest
    public void finish() {
        driver.quit();
    }
}
