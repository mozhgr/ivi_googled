package testcases;

import org.testng.annotations.Test;

public class TC3 extends BaseTest {


    /**
     * Тест "На первых 5 страницах находит ссылку на статью в wikipedia об ivi, переходит на неё и проверяет ссылку на ivi"
     */
    @Test
    public void checkWikiRedirection() {

        //поиск 'ivi' в google
        searchIvi();

        //поиск на первых 5 страницах ссылки на статью в wikipedia об ivi и переход на неё
        searchWikiOnFirstFivePages();

        //проверка наличия ссылки на ivi.ru
        checkIviLinkExistence();

    }

}
