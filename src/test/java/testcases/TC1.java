package testcases;

import org.testng.annotations.Test;

public class TC1 extends BaseTest {


    /**
     * Тест "Не менее 3 картинок в выдаче ведут на сайт ivi.ru"
     */
    @Test
    public void checkRedirectionToIviSite() {

        //поиск 'ivi' в google
        searchIvi();
        openSearchPage();

        //переход на страницу 'Картинки'
        goToPicks();
        openPicsPage();

        //нажатие на кнопку 'Инструменты'
        clickInstrumentsButton();

        //нажатие на кнопку 'Размер'
        clickButtonSize();

        //нажатие на кнопку 'Большие'
        clickLargeSizeButton();

        //поиск ссылок на ivi.ru на первой странице результатов поиска
        searchIviInLinksList();

    }

}
