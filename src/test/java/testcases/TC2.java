package testcases;

import org.testng.annotations.Test;

public class TC2 extends BaseTest {


    /**
     * Тест "Рейтинг приложения на кратком контенте страницы совпадает с рейтингом при переходе"
     */
    @Test
    public void checkGooglePlayRating() {

        //поиск 'ivi' в google
        searchIvi();

        //поиск на первых 5 страницах ссылки на приложение ivi в play.google.com и переход по ней
        searchGooglePlayOnFirstFivePages();

        //сравнение рейтинга
        compareGooglePlayIviRating();
    }

}
