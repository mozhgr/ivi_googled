Системные требования:
    Для запуска необходима последняя версия хрома 79(желательно) или 80.
    Если хром 80-й, то заменить в файле ivi_googled\src\main\resources\config.properties
    строку chromedriver.exe на chromedriver80.exe.
    Если используется какая-то другая версия браузера chrome, то:
     1. скачать необходимую версию дравйвера с https://chromedriver.chromium.org/downloads
     2. заменить файл src\\test\\resources\\chromedriver.exe на скачанный

Для запуска кейсов необходимо:
 1. импортировать проект через IntellijIDEA (File -> New -> Project from Version Control -> Git ->
    <вставить ссылку https://mozhgr@bitbucket.org/mozhgr/ivi_googled.git в поле URL>)
 2. в эксплорере проекта IDEA перейти в папку ivi_googled\src\test\java\testcases
 3. нажать во всплывающем окне IDEA (снизу справа) Enable Auto Import, чтобы импортировать зависимости maven
 4. запустить необходимый кейс нажатием правой кнопкой мыши по кейсу (TC1, TC2 или TC3) и нажатием пункта Run
